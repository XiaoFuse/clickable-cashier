# Cashier

A mobile companion for Ledger plain-text-accounting

[![OpenStore](https://open-store.io/badges/en_US.svg)](https://open-store.io/app/cashier.alensiljak)

The application repository is [here](https://gitlab.com/alensiljak/cashier).

## Description

Cashier can be used offline to enter transactions and export them for entry into a Ledger book. 
In addition to the standard Ledger transaction entry and related entity features, it contains a system for scheduling transactions, asset allocation module.
The app can be synchronized via a companion application CashierSync, which is available in Python via Pip.

## Changelog

v2021.03.25

Export to pCloud.
Link to the web app, to make updates seamless.

v2021.03.22

Fixing the logo path.

v2021.03.06-1

The first release on Open Store. A fairly stable and complete version.

## License

Copyright (C) 2021  Alen Siljak

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
